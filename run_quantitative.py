import numpy as np
import cv2
import torch
import imutils
import dlib
import os
import random
import torch.nn as nn
import tensorflow as tf
import datetime
import config
from helpers import normalize
from torchsummary import summary
from torch.utils.data import Dataset, DataLoader
from tqdm import tqdm
from torch.utils.tensorboard import SummaryWriter
from AlignedReID.aligned_reid.model.Model import Model
from gallery_management import get_distance, gallery_management
from dataset import ReID_Dataset, get_split


def test_AlignedReID():
    """
    Code for quantitative evaluation of our system on the Market_1501 dataset.
    """

    # load aligned reid network
    network = Model(num_classes=751)
    state = torch.load(config.MODEL_WEIGHT_ALIGNED)
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    network.load_state_dict(state)
    network.to(device).double()
    network.eval()

    # set up tensorboard summary writer
    current_time = str(datetime.datetime.now().timestamp())
    test_log_dir = 'logs/tensorboard/test/' + current_time
    test_summary_writer = tf.summary.create_file_writer(test_log_dir)

    # dataloaders
    gallery_df, query_df, unknowns = get_split(os.path.join(
        os.getcwd(), "datasets", "Market_1501"), "gallery.csv", "query.csv", config.NO_SHOTS, config.UNKNOWNS)
    gallery_dataloader = DataLoader(ReID_Dataset(gallery_df, os.path.join(
        os.getcwd(), "datasets", "Market_1501", "bounding_box_test")), batch_size=1)  # increase batch size for faster processing
    query_dataloader = DataLoader(ReID_Dataset(query_df, os.path.join(
        os.getcwd(), "datasets", "Market_1501", "query")), batch_size=1)  # increase batch size for faster processing
    gallery = dict()
    id_count = 0

    # keep track of accuracies
    correct_rank1 = 0
    correct_rank5 = 0
    correct_rank10 = 0
    total = 0

    # eval loop
    with torch.no_grad():
        # create gallery of known identities
        for i, batch in enumerate(tqdm(gallery_dataloader)):
            sample = batch
            global_feat, local_feat, logits = network.forward(sample['image'].to(device).double())
            if sample['label'] not in gallery.keys():
                gallery[sample['label']] = []
            # normalize for reasonable euclidean distance computing
            global_feat = normalize(global_feat.data.cpu().numpy())
            gallery[sample['label']].append(global_feat)

        # evaluate model
        for i, batch in enumerate(query_dataloader):
            sample = batch
            global_feat, local_feat, logits = network.forward(sample['image'].to(device).double())
            # normalize for reasonable euclidean distance computing
            global_feat = normalize(global_feat.data.cpu().numpy())
            rank1, rank5, rank10 = get_distance(global_feat, gallery)
            gallery, min_id = gallery_management(
                global_feat, gallery, rank1[0], rank1[1], config.THRESH, config.NO_SHOTS)

            # determine rank1 accuracy
            if min_id == -1:
                if sample['label'] in unknowns:
                    correct_rank1 += 1
                    correct_rank5 += 1
                    correct_rank10 += 1
                else:
                    pass
            else:
                if min_id == sample['label']:
                    correct_rank1 += 1

                # determine rank5 accuracy
                if sample['label'] in rank5[1]:
                    correct_rank5 += 1

                # determine rank10 accuracy
                if sample['label'] in rank10[1]:
                    correct_rank10 += 1

            total += 1

            # This is where I'm recording to Tensorboard
            with test_summary_writer.as_default():
                tf.summary.scalar('rank1', correct_rank1 / total, step=i)
                tf.summary.scalar('rank5', correct_rank5 / total, step=i)
                tf.summary.scalar('rank10', correct_rank10 / total, step=i)

            print("[TEST] %i/%i: The rank1 accuracy is: %f" %
                  (i, len(query_dataloader), (correct_rank1 / total)))
            print("The rank5 accuracy is: %f" % (correct_rank5 / total))
            print("The rank10 accuracy is: %f" % (correct_rank10 / total))

        writer.flush()


if __name__ == "__main__":
    test_AlignedReID()
