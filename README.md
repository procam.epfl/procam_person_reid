# ProCam Person Re-ID
This project realizes the person re-identification for ProCam using Python, i.e. it allows to re-identify persons in videos by assigning them a unique ID. 

## Processing Pipeline
The processing pipeline works as follows:

1. Persons are identified using a trained version of the YoloV3.
2. A correlation tracker is implemented using dlib to keep track of the detected persons. This is done for performance reasons since running the object detector of step 1 at every frame is expensive.
3. The preprocessed frames are then forwarded through the Aligned Re-ID network which gives us an ID for each person in the scene.
4. Last but not least, gallery matching is performed by looking for the closest feature vector in the gallery. If all feature vectors are larger than the passed threshold, we create a new ID.

All pretrained weights should be availble in the repository in form of LFS files, which have to be explicitly downloaded (they are not downloaded if you just pull the repository).

## Execution
To run the re-ID module on pre-recorded videos, run 
`python3 run.py --input "RELATIVE_PATH_TO_INPUT_VIDEO" --output "RELATIVE_PATH_TO_DESTIANTON" --thresh 0.42`.

"RELATIVE_PATH_TO_INPUT_VIDEO" is the relative to the pre-recorded video file which should be processed, ideally located in the [data folder](data). "RELATIVE_PATH_TO_DESTIANTON" refers to the relative path where the processed video should be stored, ideally in the [result folder](results).
The threshold is passed for the gallery retrieval. Its default is set to 0.42 in case you don't pass any argument.

In case you don't have any GPU available, you can also directly import the Google Colab script [procam_reid.ipynb](procam_reid.ipynb) and run it in Google Colab.
