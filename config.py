"""This module contains all configuration settings for reID module"""

import os
import sys


MODEL_WEIGHT_ALIGNED = os.path.join(os.getcwd(), "models", "model_weight.pth")
MODEL_MOBILE_NET_SSD_PROTOTEXT = os.path.join(
    os.getcwd(), "models", "mobilenet_ssd", "MobileNetSSD_deploy.prototxt")
MODEL_MOBILE_NET_SSD_CAFFE = os.path.join(
    os.getcwd(), "models", "mobilenet_ssd", "MobileNetSSD_deploy.caffemodel")
MODEL_YOLOV3_WEIGHTS = os.path.join(os.getcwd(), "models", "yolov3.weights")
MODEL_YOLOV3_CFG = os.path.join(os.getcwd(), "models", "yolov3.cfg")
PATH_CLASSES_COCO = os.path.join(os.getcwd(), "models", "coco.names")

# classes for mobile net ssd classification
CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat",
           "bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
           "dog", "horse", "motorbike", "person", "pottedplant", "sheep",
           "sofa", "train", "tvmonitor"]

# classes from Microsoft coco
CLASSES_COCO = None
with open(PATH_CLASSES_COCO, 'r') as f:
    CLASSES_COCO = [line.strip() for line in f.readlines()]

# Params for qualitative analysis
CONFIDENCE = 0.7  # confidence value for cv2 object detector
FRAME_DROP = 1  # process every FRAME_DROP'th frame for real-time deployment

# Params for quantitattive analysis
NO_SHOTS = 200
THRESH = 0.8
UNKNOWNS = 100
