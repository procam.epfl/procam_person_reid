"""Module for running person detection -> tracking -> re-identification"""

import numpy as np
import cv2
import torch
import imutils
import dlib
import os
import random
import torch.nn as nn
import config
import argparse
from torchsummary import summary
from AlignedReID.aligned_reid.model.Model import Model
from helpers import transform, normalize
from tqdm import tqdm
from gallery_management import get_closest_id


def main_AlignedReID(args):
    """
    Main loop for the running the aligned ReID network on video data.

    :param args:    input arguments, i.e. relative path to input video file and
                    relative path to putput video file
    """

    network = Model(num_classes=751)
    state = torch.load(config.MODEL_WEIGHT_ALIGNED)
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    network.load_state_dict(state)
    network.to(device).double()
    network.eval()

    detector = cv2.dnn.readNet(config.MODEL_YOLOV3_WEIGHTS, config.MODEL_YOLOV3_CFG)

    cap = cv2.VideoCapture(os.path.join(os.getcwd(), args.input))
    frame_count_total = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    frame_width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)  # float
    frame_height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)  # float

    # Define the codec and create VideoWriter object
    video_writer = cv2.VideoWriter(os.path.join(
        os.getcwd(), args.output), cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), 30, (600, 450))
    frame_counter = 0

    # keep track of trackers and IDs
    trackers = []
    IDs = []
    gallery = dict()
    id_count = 0
    pbar = tqdm(total=frame_count_total)

    with torch.no_grad():
        while(cap.isOpened()):
            ret, frame = cap.read()
            if frame is None:
                break
            if frame_counter % config.FRAME_DROP == 0:  # process every 10th frame for real time deployment
                frame = imutils.resize(frame, width=600)
                frame_copy = frame.copy()
                rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)  # dlib requires rgb

                if len(trackers) == 0 or frame_counter % 40 == 0:  # rerun detection every 40 seconds

                    trackers = []
                    IDs = []
                    (h, w) = frame.shape[:2]
                    detector.setInput(cv2.dnn.blobFromImage(
                        frame, 0.00392, (416, 416), (0, 0, 0), True, crop=False))
                    layer_names = detector.getLayerNames()
                    output_layers = [layer_names[i[0] - 1]
                                     for i in detector.getUnconnectedOutLayers()]
                    detections = detector.forward(output_layers)

                    for out in detections:
                        for detection in out:
                            scores = detection[5:]
                            class_id = np.argmax(scores)
                            if config.CLASSES_COCO[class_id] != "person":
                                continue
                            confidence = scores[class_id]
                            if confidence > config.CONFIDENCE:
                                center_x = int(detection[0] * w)
                                center_y = int(detection[1] * h)
                                width = int(detection[2] * w)
                                height = int(detection[3] * h)
                                x = center_x - width // 2
                                y = center_y - height // 2

                                t = dlib.correlation_tracker()
                                rect = dlib.rectangle(x, y, x + width, y + height)
                                # start tracking the object
                                t.start_track(rgb, rect)
                                trackers.append(t)
                                IDs.append(-1)

                else:
                    # update tracker positions only
                    for i in range(len(trackers)):
                        t = trackers[i]
                        t.update(rgb)

                for i in range(len(trackers)):  # iterate over all persons
                    pos = trackers[i].get_position()
                    startX = int(pos.left())
                    startY = int(pos.top())
                    endX = int(pos.right())
                    endY = int(pos.bottom())

                    # Forward pass every 40 frames to make predictions less noisy
                    # if frame_counter % 40 == 0:
                    # normalize to range 0, 1
                    rgb = cv2.normalize(rgb, None, alpha=0, beta=1,
                                        norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)

                    (h, w) = frame.shape[:2]
                    # tracking might run out of image, avoid empty crop
                    person = rgb[max(0, startY):min(endY, h), max(0, startX):min(endX, w)]
                    person = cv2.resize(person, (224, 224))
                    person = transform(np.reshape(person, [3, 224, 224]),
                                       [0.486, 0.459, 0.408], [0.229, 0.224, 0.225])

                    input = torch.unsqueeze(torch.from_numpy(person), 0)
                    global_feat, local_feat, logits = network.forward(input.to(device).double())
                    gallery, id_count, id = get_closest_id(
                        global_feat.data.cpu().numpy(), gallery, id_count, config.THRESH, config.NO_SHOTS)

                    cv2.rectangle(frame_copy, (int(startX), int(startY)),
                                  (int(endX), int(endY)), (0, 255, 0))
                    cv2.putText(frame_copy, str(id), (int(startX), int(
                        startY + 10)), cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 255, 0), 2)

                if video_writer is not None:
                    video_writer.write(frame_copy)
                else:
                    print("Video writer is None.")

            frame_counter += 1
            if frame_counter == 10000:  # start counting from 0 again after the 10k'th frame to avoid overflow
                frame_counter = 0
            pbar.update(1)

    cap.release()
    video_writer.release()
    pbar.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--input', type=str, help='relative path to input video file')
    parser.add_argument('--output', type=str, help='relative path to output video file')
    args = parser.parse_args()

    if args.input is None:
        print("Pleasee provide the relative path to the input video which should be processed.")

    if args.output is None:
        print("Pleasee provide the relative path to the output video file wherethe processed video should be stored.")

    main_AlignedReID(args)
