import cv2
import os
import pandas as pd
import numpy as np
import random
import torch
from torch.utils.data import Dataset
from helpers import transform


def get_split(root_dir, gallery_csv_name, query_csv_name, m, no_unknowns):
    """
    Set up evaluation datasets by removing a subset of unknowns from the gallery.

    :param root_dir:          path to root directory where dataset is located
    :param gallery_csv_name:  file name of csv file for gallery dataset
    :param query_scv_name:    file name of csv file of query dataset
    :param m:                 number of shot kept in gallery per identity
    :param no_unknowns:       number of unknowns for the hold out dataset

    :return:                  dataframe for gallery, dataframe for query, hold out IDs
    """
    # load the query and gallery dataframes
    df_query = pd.read_csv(os.path.join(root_dir, query_csv_name), index_col=0, header=0)
    df_gallery = pd.read_csv(os.path.join(root_dir, gallery_csv_name), index_col=0, header=0)

    print("The number of unique identities in the gallery is: " + str(df_gallery["id"].nunique()))

    # select query_size IDs which should not be in the gallery
    ids = df_query["id"].to_numpy()
    unknown_ids = np.random.choice(ids, no_unknowns)
    # df_query_max = df_query["id"].max()
    # unknown_ids = random.sample(range(0, int(df_query_max)), no_unknowns)

    for id in unknown_ids:
        # remove those identities from the gallery
        df_gallery = df_gallery[df_gallery["id"] != id]
        # keep only one shot of unknown
        df_id = df_query[df_query["id"] == id]
        df_query = df_query[df_query["id"] != id]
        df_id = df_id.head(1)
        df_query = df_query.append(df_id, ignore_index=True)

    print("After removing unknown identities, the number of unique identities in the gallery is: " +
          str(df_gallery["id"].nunique()))
    print("The length of the gallery is: " + str(len(df_gallery)))

    # keep only m shots of remaining identities in gallery
    for id in df_gallery["id"]:
        no_stored_shots = random.sample(range(1, m), 1)[0]
        df_id = df_gallery[df_gallery["id"] == id]
        df_gallery = df_gallery[df_gallery["id"] != id]
        df_id = df_id.head(no_stored_shots)
        df_gallery = df_gallery.append(df_id, ignore_index=True)

    print("Afer keeping only m shots, the number of unique identities in the gallery is: " +
          str(df_gallery["id"].nunique()))
    print("The length of the galler is: " + str(len(df_gallery)))

    return df_gallery, df_query, unknown_ids


class ReID_Dataset(Dataset):
    """ReID dataset."""

    def __init__(self, df, root_dir):
        """
        :param df:        dataframe
        :param root_dir:  path to root directory of dataset where images are located
        """
        self.df = df
        self.root_dir = root_dir

    def __len__(self):
        return len(self.df)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        img_name = os.path.join(self.root_dir, self.df.iloc[idx, 0])
        image = cv2.imread(img_name)
        image = cv2.normalize(image, None, alpha=0, beta=1,
                              norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)
        image = cv2.resize(image, (224, 224))
        image = transform(np.reshape(image, [3, 224, 224]), [
                          0.486, 0.459, 0.408], [0.229, 0.224, 0.225])

        label = self.df.iloc[idx, 1]
        sample = {'image': image, 'label': label}
        return sample
