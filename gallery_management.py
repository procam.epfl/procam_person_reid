import numpy as np
import cv2
import random
from sklearn.ensemble import IsolationForest


def detect_outlier(data, m):
    """
    Detects outliers in shots of identity by performing PCA and then applying
    L2 based outlier detection.

    :param data:  feature vectors for one identity in gallery, normalized

    :return:      array of size as input, -1 corresponding to outlier, 1 to inlier
    """
    data = np.asarray(data)
    vec_count, _, feature_size = data.shape
    data = np.resize(data, (vec_count, feature_size))
    #pca = PCA(n_components=2)
    #data = pca.fit_transform(data)
    # TODO: replace isolationForest
    clf = IsolationForest(behaviour='new', max_samples=m, random_state=1, contamination='auto')
    preds = clf.fit_predict(data)
    return preds


def sort_by_dist(dists, ids):
    """
    Sorts the identity labels by their distances to the query vector.
    """
    dists_ids = zip(ids, dists)
    # sort ids by distance
    dists_ids_sorted = sorted(dists_ids, key=lambda x: x[1])
    ids, dists = map(list, zip(*dists_ids_sorted))
    return dists, ids


def get_distance(feature_vec, gallery):
    """
    Gallery management and finding the closest ID

    :param feature_vec:   feature vector of query ID
    :param gallery:       gallery of known identities

    :return:              minimum distance to one identitiy in gallery,
                          label of that identity
    """
    min_dist = float('Inf')
    min_id = -1
    rank5_dist = []
    rank5_ids = []
    rank10_dist = []
    rank10_ids = []

    for id in gallery.keys():
        dist = 0
        #cur_outliers = detect_outlier(gallery[id])
        for idx in range(len(gallery[id])):
            # if cur_outliers[idx] == -1:
            #  dist += 0.5 * np.linalg.norm(feature_vec - gallery[id][idx])
            # else:
            #  dist += np.linalg.norm(feature_vec - gallery[id][idx])
            dist += np.linalg.norm(feature_vec - gallery[id][idx])
        dist /= len(gallery[id])  # avg distance

        # rank1
        if dist < min_dist:
            min_dist = dist
            min_id = id

        # rank5
        if len(rank5_dist) < 5:
            rank5_dist.append(dist)
            rank5_ids.append(id)
        else:
            rank5_dist, rank5_ids = sort_by_dist(rank5_dist, rank5_ids)
            if dist < rank5_dist[-1]:
                rank5_dist[-1] = dist
                rank5_ids[-1] = id

        # rank10
        if len(rank5_dist) < 10:
            rank10_dist.append(dist)
            rank10_ids.append(id)
        else:
            rank10_dist, rank10_ids = sort_by_dist(rank10_dist, rank10_ids)
            if dist < rank10_dist[-1]:
                rank10_dist[-1] = dist
                rank10_ids[-1] = id

    return (min_dist, min_id), (rank5_dist, rank5_ids), (rank10_dist, rank10_ids)


def gallery_management(feature_vec, gallery, min_dist, min_id, thresh, m):
    """
    Quantitative analysis
    Gallery management, i.e. replacing outliers

    :param feature_vec:   feature vector of query
    :param gallery:       gallery of known identities
    :param min_dist:      minimum distance established between query and one identity in gallery
    :param min_id:        label for identity corresponding to that minimum distance
    :param thresh:        threshold for decision rule in novelty detection, default is 0.42
    :param m:             number of shots kept per identity in the gallery, default is 5
    """

    if min_dist > thresh:
        # new ID, just show that new ID was detected
        return gallery, -1
    else:
        # update gallery: store feature vector which is the average of all feature vectors assigned with this ID
        if len(gallery[min_id]) >= m:
            # replace random feature vec from gallery
            cur_outliers = detect_outlier(gallery[min_id], m)
            # replace one of the outliers
            idcs = np.where(cur_outliers == -1)[0]
            if len(idcs) == 0:
                idx = random.randrange(0, m)
            else:
                idx = np.random.choice(idcs)
            gallery[min_id][idx] = feature_vec
        else:
            gallery[min_id].append(feature_vec)

        return gallery, min_id


def get_closest_id(feature_vec, gallery, id_count, thresh, m):
    """
    Qualitative
    Gallery management and finding the closest ID, i.e. creating the gallery on
    the fly. For each ID, we keep track of 15 reference feature vectors. If new
    feature vectors come in, we randomly replace one of the stored vectors to
    give more recent feature vectors more importance.

    :param feature_vec: query feature vector, i.e. global feature produces by
                        AlignedReID network
    :param gallery:     gallery we have as reference
    :param id_count:    number of ids available
    :param thresh:      threshold theta for creating a new ID

    :return:            updated gallery, number of IDs available, the id assigned
    """
    min_dist = float('Inf')
    min_id = -1
    feature_vec = normalize(feature_vec)

    for id in range(1, id_count):
        dist = 0
        for ref_vec in gallery[id]:
            dist += np.linalg.norm(feature_vec - ref_vec)
        dist /= len(gallery[id])  # avg distance

        if dist < min_dist:
            min_dist = dist
            min_id = id

    if min_dist > thresh or id_count == 0:
        gallery[id_count + 1] = []
        gallery[id_count + 1].append(feature_vec)
        return gallery, id_count + 1, id_count + 1
    else:
        if len(gallery[min_id]) >= m:
            # replace random feature vec from gallery
            cur_outliers = detect_outlier(gallery[min_id])
            # replace one of the outliers
            idcs = np.where(cur_outliers == -1)[0]
            if len(idcs) == 0:
                idx = random.randrange(0, m)
            else:
                idx = np.random.choice(idcs)
            gallery[min_id][idx] = feature_vec
        else:
            gallery[min_id].append(feature_vec)
        return gallery, id_count, min_id
